<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "myDB";

// Create connection
$connection = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
} 
echo "Connected successfully";

// Create database
/* $sql = "CREATE DATABASE myDB";
if ($connection->query($sql) === TRUE) {
    echo "Database created successfully";
} else {
    echo "Error creating database: " . $connection->error;
} */

/*$sqlQuery = "CREATE TABLE projects1(
    id INT AUTO_INCREMENT,
    title VARCHAR(100) NOT NULL,
    description TEXT,
    created_by INT,
    status INT,
    PRIMARY KEY(id))";

    if($connection->query($sqlQuery) === TRUE) {
        echo "Table created successfully";
    } else {
        echo "<br>Unable to create table<br>". $connection->error;
    }
*/

/*$title = "proj2";
$description = "trial proj2";
$createdBy = "";
$status = 101;
 $sqlQuery = "INSERT INTO projects1 (title, description, status) VALUES ( '$title', '$description', $status)";

       if ($connection->query($sqlQuery)) {
           $message = "Insert successful";
       } else {
           $message = "Unsuccessful<br>". $connection->error;
	   }
	   echo $message; */
	   
	   
// SQL to select table rows
$sql = "SELECT title, description, status FROM projects1";
$result = $connection->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "title: " . $row["title"]. " - description: " . $row["description"]. "status " . $row["status"]. "<br>";
    }
} else {
    echo "0 results";
} 

//sql to update table 
/* $sql = "UPDATE projects1 SET status=104 WHERE id=2";

if ($connection->query($sql) === TRUE) {
    echo "Record updated successfully";
} else {
    echo "Error updating record: " . $conn->error;
}
 */

// sql to delete a record
$sql = "DELETE FROM projects1 WHERE status=101";

if ($connection->query($sql) === TRUE) {
    echo "Record deleted successfully";
} else {
    echo "Error deleting record: " . $conn->error;
}


$connection->close();
?>